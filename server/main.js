"use strict";

var express = require('express');
var Wordpos = require('wordpos');
var wordpos = new Wordpos();

var app = express();

app.use(function (req, res, next) {
    console.log(req.url);
    next();
});

app.get('/word/:word', function (req, res) {
    let word = req.params.word.toLowerCase();
    wordpos.lookup(word, (result) => {
        var response = result.map(res => {
            return {
                word,
                lexName: res.lexName,
                def: res.def,
                exp: res.exp
            }
        })
        console.log(response);
        res.json(response);
    });
});

var listener = app.listen(9037, () => {
    console.log(`Example app listening on port ${listener.address().port}`);
});
