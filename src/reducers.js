import { combineReducers } from 'redux';
import * as db from './db.js';

import { Alert, NavigationExperimental } from 'react-native';
const { StateUtils: NavigationStateUtils } = NavigationExperimental;

const generateDefaultState = function () {
    return {
        user: null,
        word: null,
        wordResults: null,
        drawerOpen: false,
        lookupWordModalVisible: false,
        words: [],
        mode: null,
        navigationState: {
            index: 0,
            routes: [{key: 'review'}, {key: 'result'}],
        },
        isConnected: true,
        confirmModalState: {show: false},
        requests: {},
    }
};

const NAV_MAP = {
    REVIEW: 0,
    RESULT: 1,
};

const reducer = function (state = generateDefaultState(), action) {
    switch (action.type) {
        case 'SET_REQUEST_ACTIVE':
            state.requests = Object.assign({}, state.requests, {[action.request]: true});
            break;
        case 'SET_REQUEST_COMPLETE':
            state.requests = Object.assign({}, state.requests);
            delete state.requests[action.request];
            break;
        case 'CLEAR_USER':
            state = generateDefaultState();
            break;
        case 'SET_USER':
            state.user = action.user;
            break;
        case 'CLEAR_WORD_RESULTS':
            state.wordResults = null;
            state.navigationState = NavigationStateUtils.jumpToIndex(state.navigationState, NAV_MAP.REVIEW);
            break;
        case 'SET_WORD_RESULTS':
            state.wordResults = action.wordResults;
            state.navigationState = NavigationStateUtils.jumpToIndex(state.navigationState, NAV_MAP.RESULT);
            break;
        case 'SET_WORDS':
            state.words = action.words;
            break;
        case 'SET_CONNECTION_STATE':
            if (action.state && !state.isConnected) {
                Alert.alert('Alert', 'Word Watch is no longer in offline mode.');
                state = generateDefaultState();
            }
            if (!action.state && state.isConnected) {
                Alert.alert('Warning', 'You are currently not connected to the internet. Word Watch will now operate in offline mode.');
                state = generateDefaultState();
            }
            state.isConnected = action.state;
            break;
        case 'TOGGLE_DRAWER':
            state.drawerOpen = !state.drawerOpen;
            break;
        case 'CLOSE_DRAWER':
            state.drawerOpen = false;
            break;
        case 'OPEN_DRAWER':
            state.drawerOpen = true;
            break;
        case 'SHOW_LOOKUP_WORD_MODAL':
            state.lookupWordModalVisible = true;
            break;
        case 'HIDE_LOOKUP_WORD_MODAL':
            state.lookupWordModalVisible = false;
            break;
        case 'SHOW_CONFIRM_MODAL':
            state.confirmModalState = {
                show: true,
                text: action.text,
                cb: action.cb,
            };
            break;
        case 'HIDE_CONFIRM_MODAL':
            state.confirmModalState = {show: false};
            break;
    }

    return Object.assign({}, state);
};

//app = combineReducers({
//
//});

export default reducer;
