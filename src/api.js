import { Alert } from 'react-native';
import { Word } from './Word.js';

var HOST = '192.168.1.11';
var PORT = '9037';

const formatWordResult = function (result) {
    let word = {};

    word.word = result[0].word;
    word.definitions = result.reduce((prev, curr) => {
        let partOfSpeech = curr.lexName.split('.')[0];
        prev[partOfSpeech] = prev[partOfSpeech] || [];
        prev[partOfSpeech].push(curr.def[0].toUpperCase() + curr.def.slice(1));
        return prev;
    }, {});

    return word;
}

export const lookup = function (word) {
    return fetch(`http://${HOST}:${PORT}/word/${word}`)
        .then((response) => response.json())
        .then((result) => {
            if ( ! result.length ) return null;
            let word = formatWordResult(result);
            return new Word(word.word, word.definitions);
        })
        //.then((wordResults) => app.dispatch({type: 'SET_WORD_RESULTS', wordResults}))
        .catch((err) => {
            Alert.alert('Error...', err.message);
        });
}
