import * as firebase from 'firebase';
import { AsyncStorage } from 'react-native';
import { Word } from './Word.js';

var config = { 
    apiKey: "AIzaSyAWWpNFn9Fy3dfkjx5PEihtnFNYv7XDcaM",
    authDomain: "word-watch.firebaseapp.com",
    databaseURL: "https://word-watch.firebaseio.com",
    storageBucket: "word-watch.appspot.com",
};
firebase.initializeApp(config);

const genWordsKey = function (user) {
    return 'WordWatch:' + user.email + ':Words';
}

const cacheFetchedWords = function (user, words, cb) {
    words = words.map(word => word.toCacheable());
    AsyncStorage.setItem(genWordsKey(user), JSON.stringify(words), (err) => {
        if (err) throw err;
        cb();
    });
}

const fetchCachedWords = function (user, cb) {
    AsyncStorage.getItem(genWordsKey(user), (err, result) => {
        if (err) throw err;
        result = JSON.parse(result);
        result.map(word => {
            return new Word(word.word, word.definitions, word.key);
        });
        cb(result);
    });
}

const offlineLogin = function (email, cb) {
    AsyncStorage.getAllKeys((err, keys) => {
        if (err) throw err;

        let cachedUserEmails = keys.filter(key => {
            return key.indexOf('WordWatch') == 0;
        }).map(key => {
            return key.split(':')[1];
        });

        if ( cachedUserEmails.indexOf(email) > -1 ) cb(null, {email});
        else cb(new Error('Cached data for ' + email + ' was not found on this device.'));
    });
};

export const fetchWords = function (user, isConnected) {
    return new Promise((resolve, reject) => {
        if ( !isConnected ) return fetchCachedWords(user, (words) => {
            resolve(words);
        });

        if ( ! user ) throw new Error('User currently not logged in');
        var ref = firebase.database().ref('/user/' + user.uid + '/words');
        ref.on('value', (snapshot, err) => {
            if (err) throw err;

            var words = [];
            snapshot.forEach(word => {
                let { key } = word;
                word = word.val();
                words.push(new Word(word.word, word.definitions, key));
            });
            cacheFetchedWords(user, words, () => {
                resolve(words);
            });
        });
    });
}

function subscribeWords(user) {

}

export const login = function (email, password, isConnected) {
    return new Promise((resolve, reject) => {
        if ( ! isConnected ) return offlineLogin(email, (err, user) => {
            if (err) reject(err);
            else resolve(user);
        });

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(resolve)
        .catch(reject);
    });
}

export const register = function (email, password) {
    return new Promise((resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password) 
        .then(resolve)
        .catch(reject);
    });
}

export const saveWord = function (user, word) {
    return new Promise((resolve, reject) => {
        var ref = firebase.database().ref('/user/' + user.uid + '/words');
        ref.push(word.toObject(), err => {
            if (err) return reject(err);
            resolve();
        });
    });
}

export const removeWord = function (user, word) {
    return new Promise((resolve, reject) => {
        let { key } = word;
        if ( ! key ) Alert.alert('Error...', 'Key missing from word. Could not remove.');

        var ref = firebase.database().ref('/user/' + user.uid + '/words/' + key);
        ref.remove()
        .then(() => resolve())
        .catch(reject);
    });
}
