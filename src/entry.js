import { AppRegistry, BackAndroid } from 'react-native';
import { RootComponent } from './components/WWComponents.js';

// What can we do with the the back button?
//BackAndroid.addEventListener('hardwareBackPress', () => {
//    return true;
//});

AppRegistry.registerComponent('WordWatchNativeScript', () => RootComponent);
