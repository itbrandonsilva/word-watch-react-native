"use strict";

import { StyleSheet, Navigator, Dimensions } from 'react-native';

let {height, width} = Dimensions.get('window');

let styles = StyleSheet.create({
    home: {
        zIndex: 2000,
    },
    homeLogoContainer: {
        width,
        height: height/2,
        backgroundColor: 'teal',
    },
    // No idea why this works the way it does...but these styles are necessary for resizeMode='contain' to work
    homeLogo: {
        flex: 1,
        width: width/2,
    },
    homeInputContainer: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    registerHeader: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 20,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInput: {
    
    },
    button: {
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    flexCenter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexCenterVertical: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    flexCenterHorizontal: {
        flex: 1,
        justifyContent: 'center',
    },
    navBarPaddingTop: {
        paddingTop: Navigator.NavigationBar.Styles.General.NavBarHeight,
    },
    navBarMarginTop: {
        marginTop: Navigator.NavigationBar.Styles.General.NavBarHeight,
    },
    navBarText: {
        color: 'white',
        fontSize: 20,
    },
    modal: {
        padding: 20,
        backgroundColor: 'teal',
        width: width/1.3,
    },
    modalText: {
        color: 'white',
        fontSize: 20,
    },
});

let drawerStyles = {
    drawer: {
        top: Navigator.NavigationBar.Styles.General.NavBarHeight,
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        backgroundColor: 'teal',
    },
    main: {
    }
};

export {styles, drawerStyles, width, height};
