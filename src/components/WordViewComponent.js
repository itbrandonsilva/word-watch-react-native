import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showConfirmModal, saveWord, removeWord } from '../actions.js';
import {
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from '../styles.js';

export class WordViewComponent extends Component {
    confirmDeletion() {
        this.props.showConfirmModal('Are you sure you want to remove \'' + this.props.word.word + '\'?', (confirm) => {
            if (confirm) this.props.removeWord(this.props.word);
        });
    }

    render() {
        let { word } = this.props;
        if ( ! word ) return null;

        let views = [];
        Object.keys(word.definitions).forEach((partOfSpeech, idx) => {
            var definitionViews = [];
            let definitions = word.definitions[partOfSpeech];
            definitions.forEach((definition, idx) => {
                definitionViews.push(
                    <Text key={idx}>- { definition }</Text>
                );
            });
            views.push(
                <View key={idx} style={{margin: 15, marginBottom: 0}}>
                    <Text style={{fontSize: 22}}>
                        { partOfSpeech }
                    </Text>
                    { definitionViews }
                </View>
            )
        });

        let saveBtn = this.props.enableSave ? (
            <View>
                <TouchableOpacity
                    onPress={() => this.props.saveWord(this.props.word)}
                    style={styles.icon}>
                    <Text>
                        <Icon name={'save'} size={30} color={'teal'} />
                    </Text>
                </TouchableOpacity>
            </View>
        ) : null;

        let removeBtn = !this.props.enableSave && this.props.isConnected ? (
            <View>
                <TouchableOpacity
                    onPress={this.confirmDeletion.bind(this)}
                    style={styles.icon}>
                    <Text>
                        <Icon name={'remove'} size={30} color={'#ff5050'} />
                    </Text>
                </TouchableOpacity>
            </View>
        ) : null;


        return (
            <View>
                <View style={{marginTop: 20, flexDirection: 'row', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        { saveBtn }
                        <Text style={{fontSize: 30, fontWeight: 'bold'}}>
                            { word.word }
                        </Text>
                        { removeBtn }
                    </View>
                </View>
                { views }
            </View>
        );
    }
}
WordViewComponent = connect(
    state => {
        return {
            isConnected: state.isConnected,
        }
    },
    {
        saveWord,
        removeWord,
        showConfirmModal,
    }
)(WordViewComponent);
