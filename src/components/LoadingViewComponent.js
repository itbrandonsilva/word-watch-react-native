import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    View,
} from 'react-native';
import { styles } from '../styles.js';
import { SpinnerIconComponent } from './WWComponents.js';

export class LoadingViewComponent extends Component {
    render() {
        return (
            <View 
                style={[
                    styles.navBarMarginTop,
                    {paddingTop: 30, flexDirection: 'row', justifyContent: 'center'},
                ]}>
                <SpinnerIconComponent />
            </View>
        );
    }
}
LoadingViewComponent = connect(
)(LoadingViewComponent);
