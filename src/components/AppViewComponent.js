import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleDrawer, clearWordResults } from '../actions.js';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    Text,
    TouchableOpacity,
    Navigator,
    ScrollView,
    ListView,
    View,
    NavigationExperimental,
} from 'react-native';
import { configureScene } from '../util.js';
import { styles, drawerStyles } from '../styles.js';
import { NavigationTransitionerPatched, AppViewDrawerComponent, ResultViewComponent, LookupWordModalComponent, ConfirmModalComponent, NavBarComponent, WordReviewViewComponent, WordViewComponent } from './WWComponents.js';

const {
  StateUtils: NavigationStateUtils,
  Header: NavigationHeader,
  Card: NavigationCard,
} = NavigationExperimental;

export class AppViewComponent extends Component {
    /*render() {
        return (
            <ScrollView contentContainerStyle={{flex: 1}}>
                <LookupWordModalComponent />
                <ConfirmModalComponent />
                <Navigator
                    style={{flex: 1}}
                    ref={(ref) => this._navigator = ref}
                    initialRoute={{scene: 'review'}}
                    configureScene={configureScene}

                    navigationBar={NavBar}
                    renderScene={this.sceneRenderer.bind(this)} />
            </ScrollView>
        );
    }*/

    /*sceneRenderer(route, navigator) {
        return (
            <AppViewDrawerComponent>
                {(() => {
                    switch (route.scene) {
                        case 'review':
                            if (this.props.words.length) return <WordReviewViewComponent mode={this.state.mode}/>;
                            else                         return <LoadingViewComponent />;
                        case 'result':
                            return <ResultViewComponent />
                    }
                })()}
            </AppViewDrawerComponent>
        );
    }*/

    render() {
        return (
            <ScrollView contentContainerStyle={{flex: 1}}>
                <LookupWordModalComponent />
                <ConfirmModalComponent />

                <NavigationTransitionerPatched
                    //onNavigateBack={this._onPopRoute}
                    navigationState={this.props.navigationState}
                    render={this.sceneRenderer.bind(this)}
                    style={{flex: 1}}
                />
            </ScrollView>
        );
    }

    sceneRenderer(sceneProps) {
        let { scene } = sceneProps;
        let { route } = scene;
        return (
            <View style={{flex: 1}}>
                <NavigationCard
                    {...sceneProps}
                    renderScene={({ scene }) => {
                        let { route } = scene;
                        return (
                            <AppViewDrawerComponent>
                                {(() => {
                                    switch (route.key) {
                                        case 'review':
                                            return <WordReviewViewComponent />;
                                        case 'result':
                                            return <ResultViewComponent />;
                                    }
                                })()}
                            </AppViewDrawerComponent>
                        );
                    }}
                    key={route.key}
                />
                <NavigationHeader
                    {...sceneProps}
                    style={{backgroundColor: 'teal', elevation: 0}}
                    renderLeftComponent={props => {
                        let { route } = props.scene;
                        switch (route.key) {
                            case 'result':
                                return (
                                    <TouchableOpacity
                                        onPress={this.props.clearWordResults}
                                        style={styles.icon}>
                                        <Text>
                                            <Icon name="chevron-left" size={30} color="white" />
                                        </Text>
                                    </TouchableOpacity>
                                )
                            default:
                                return (
                                    <TouchableOpacity
                                        onPress={this.props.toggleDrawer}
                                        style={styles.icon}>
                                        <Text>
                                            <Icon name="bars" size={30} color="#fff" />
                                        </Text>
                                    </TouchableOpacity>
                                )
                        }
                    }}
                    renderTitleComponent={props => {
                        return (<NavigationHeader.Title textStyle={{color: 'white'}}>Word Watch</NavigationHeader.Title>);
                    }}
                />
            </View>
        )
    }
}
AppViewComponent = connect(
    state => {
        return {
            words: state.words,
            wordResults: state.wordResults,
            navigationState: state.navigationState,
        }
    },
    {
        clearWordResults,
        toggleDrawer,
    }
)(AppViewComponent);
