import React, { Component } from 'react';
import { connect } from 'react-redux';
import { hideConfirmModal } from '../actions.js';
import {
    Alert,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Modal,
} from 'react-native';
import { styles } from '../styles.js';

export class ConfirmModalComponent extends Component {
    confirm (bool) {
        this.props.state.cb(bool);
        this.props.hideConfirmModal();
    }

    render() {
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.props.state.show}
                onRequestClose={() => {}}>

                <View style={styles.flexCenter}>
                    <View style={styles.modal}>
                        <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <View style={{justifyContent: 'flex-start', alignItems: 'flex-start', marginBottom: 13}}>
                                <Text style={[styles.modalText, {textAlign: 'center'}]}>
                                    { this.props.state.text }
                                </Text>
                            </View>
                            <View style={{flex: 0.1}}></View>
                            <View style={{flex: 0.45, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                <TouchableOpacity onPress={() => this.confirm(true)} style={[styles.button, {flex: 0.5}]}>
                                    <Text>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.confirm(false)} style={[styles.button, {flex: 0.5}]}>
                                    <Text>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>

            </Modal>
        );
    }
}
ConfirmModalComponent = connect(
    state => {
        return {
            state: state.confirmModalState
        }
    },
    {
        hideConfirmModal,
    }
)(ConfirmModalComponent);
