import React, { Component } from 'react';
import { connect } from 'react-redux';
import { closeDrawer, openDrawer, clearUser, showLookupWordModal } from '../actions.js';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    Text,
    TouchableOpacity,
    View,
    ListView,
} from 'react-native';
import { styles, drawerStyles } from '../styles.js';

export class AppViewDrawerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({rowHasChanged: (a, b) => a !== b}),
        };
    }

    render() {
        return (
            <Drawer
                open={this.props.drawerOpen}
                ref={(ref) => {this._drawer = ref}}
                styles={drawerStyles}
                type={"displace"}
                content={this.renderDrawer()}
                openDrawerOffset={0.65}
                closedDrawerOffset={0}
                side={'left'}
                tweenDuration={250}
                tweenEasing={'easeOutQuint'}
                captureGestures={false}
                onOpen={this.props.openDrawer}
                onClose={this.props.closeDrawer}
                tapToClose={true}
                panCloseMask={0.65}
                panOpenMask={0.2}
                >

                { this.props.children }
            </Drawer>
        );
    }

    renderDrawer() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData) =>
                    <TouchableOpacity onPress={() => {rowData.onPress()}}
                        style={{flexDirection: 'row', height: 45}}>

                        <View style={{flex: 0.15}}></View>
                        <View style={{flex: 0.70, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 5}}>
                            <Icon name={rowData.icon} size={18} color={'white'} style={{marginRight: 10}}/>
                            <Text style={{color: 'white'}}>
                                {rowData.text}
                            </Text>
                        </View>
                        <View style={{flex: 0.15}}></View>
                    </TouchableOpacity>
                }
            />
        );
    }

    componentDidMount() {
        this.setState({
            // 'Word Watch', 'Profile', 'Settings', 'Games', 'Logout'
            dataSource: this.state.dataSource.cloneWithRows([
                {text: 'Lookup', icon: 'book', onPress: this.props.showLookupWordModal},
                {text: 'Logout', icon: 'sign-out', onPress: this.props.clearUser},
            ]),
        });
    }
}
AppViewDrawerComponent = connect(
    state => {
        return {
            drawerOpen: state.drawerOpen,
        }
    },
    {
        clearUser,
        closeDrawer,
        openDrawer,
        showLookupWordModal,
    }
)(AppViewDrawerComponent);
