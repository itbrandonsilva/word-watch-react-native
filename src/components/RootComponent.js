import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../store.js';
import { ViewSelectorComponent } from './WWComponents.js';

export class RootComponent extends Component {
    render() {
        return (
            <Provider store={store}>
                <ViewSelectorComponent />
            </Provider>
        );
    }
}
