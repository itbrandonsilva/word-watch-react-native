import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login, register } from '../actions.js';
import {
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Navigator,
    ScrollView,
    Image,
} from 'react-native';
import { configureScene } from '../util.js';
import { styles, width, height } from '../styles.js';
import { } from './WWComponents.js';

export class LoginViewComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'email@email.com',
            password: 'password',
            passwordConfirmation: 'password',
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.user) this._navigator.resetTo({scene: 'login'});
    }

    renderLogin(route, navigator) {
        let signUpButton = this.props.isConnected ? (
            <TouchableOpacity 
                onPress={() => navigator.push({scene: 'register'})}
                style={styles.button}>
                <Text>Sign Up</Text>
            </TouchableOpacity>
        ) : null;

        let passwordInput = this.props.isConnected ? (
            <TextInput
                placeholder = {'Password'}
                secureTextEntry = {true}
                editable = {true}
                maxLength = {40}
                onChangeText={ (password) => this.setState({password}) }
                value={this.state.password}/>
        ) : null;

        return (
            <ScrollView style={styles.container}>
                <View style={styles.homeLogoContainer}>
                    <View style={styles.flexCenter}>
                        <Image style={styles.homeLogo} resizeMode={Image.resizeMode.contain} source={require('../assets/logo.png')} />
                    </View>
                </View>

                <View style={styles.homeInputContainer}>
                    <TextInput
                        placeholder = {'Email Address'}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (email) => this.setState({email}) }
                        value={this.state.email}
                        onSubmitEditing={() => alert('onSubmitEditing')}/>
                    { passwordInput }

                    <View style={[styles.flexCenterHorizontal, {paddingTop: 20}]}>
                        <TouchableOpacity 
                            onPress={() => this.props.login(this.state.email, this.state.password)}
                            style={styles.button}>
                            <Text>Login</Text>
                        </TouchableOpacity>
                        { signUpButton }
                    </View>
                </View>
            </ScrollView>
        );
    }

    renderRegister(route, navigator) {
        return (
            <ScrollView style={styles.container}>
                <View style={{backgroundColor: 'teal', width, height: Navigator.NavigationBar.Styles.General.NavBarHeight}}>
                    <View style={styles.registerHeader}>
                        <Text style={styles.navBarText}>
                            Register
                        </Text>
                    </View>
                </View>
                <View style={styles.homeInputContainer}>
                    <TextInput
                        placeholder = {'Email Address'}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (email) => this.setState({email}) }
                        value={this.state.email}/>

                    <TextInput
                        placeholder = {'Password'}
                        secureTextEntry = {true}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (password) => this.setState({password}) }
                        value={this.state.password}/>

                    <TextInput
                        placeholder = {'Confirm Password'}
                        secureTextEntry = {true}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (passwordConfirmation) => this.setState({passwordConfirmation}) }
                        value={this.state.passwordConfirmation}/>


                    <View style={[styles.flexCenterHorizontal, {paddingTop: 20}]}>
                        <TouchableOpacity 
                            onPress={() => this.state.password === this.state.passwordConfirmation ? this.props.register(this.state.email, this.state.password) : alert('Passwords do not match.')}
                            style={styles.button}>
                            <Text>Create Account</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={() => navigator.pop()}
                            style={styles.button}>
                            <Text>Back</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )

    }

    renderScene(route, navigator) {
        // This could be a hack; find out why route would be undefined.
        if ( ! route ) return this.renderLogin(route, navigator);
        switch (route.scene) {
            case 'login':
                return this.renderLogin(route, navigator);
            case 'register':
                return this.renderRegister(route, navigator);
            default:
                return null;
        }
    }

    render() {
        return (
            <Navigator
                ref={(ref) => this._navigator = ref}
                initialRoute={this.props.route}
                configureScene={configureScene}

                renderScene={this.renderScene.bind(this)} />
        );
    }
}
LoginViewComponent = connect(
    (state) => {
        return {
            route: state.route,
            user: state.user,
            isConnected: state.isConnected,
        }
    },
    {
        login,
        register,
    }
)(LoginViewComponent);
