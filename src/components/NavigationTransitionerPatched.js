import {
    NavigationExperimental,
} from 'react-native';

export class NavigationTransitionerPatched extends NavigationExperimental.Transitioner {
    _onLayout(event) {
        if (this.frameChanged(event)) super._onLayout(event);
    }

    frameChanged(event) {
        const {height, width} = event.nativeEvent.layout;

        let frameChanged = (this._lastWidth !== width || this._lastHeight !== height);
        this._lastWidth = width;
        this._lastHeight = height;

        return frameChanged;
    }
}
