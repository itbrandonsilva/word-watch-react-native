import React, { Component } from 'react';
import { NetInfo } from 'react-native';
import { connect } from 'react-redux';
import { AppViewComponent, LoginViewComponent } from './WWComponents.js';

export class ViewSelectorComponent extends Component {
    // Reposition this code, at some point
    componentDidMount() {
        NetInfo.isConnected.addEventListener('change', state => {
            this.props.dispatch({type: 'SET_CONNECTION_STATE', state});
        }); 
    }

    render() {
        return ( this.props.user ? <AppViewComponent /> : <LoginViewComponent /> );
    }
}
ViewSelectorComponent = connect(
    (state) => {
        return {
            user: state.user
        }
    }
)(ViewSelectorComponent);
