import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    ScrollView,
} from 'react-native';
import { styles } from '../styles.js';
import { WordViewComponent } from './WWComponents.js';

export class ResultViewComponent extends Component {
    render() {
        return (
            <ScrollView style={styles.navBarMarginTop}>
                <WordViewComponent word={this.props.wordResults} enableSave={true} />
            </ScrollView>
        );
    }
}
ResultViewComponent = connect(
    (state) => {
        return {
            wordResults: state.wordResults,
        }
    }
)(ResultViewComponent);
