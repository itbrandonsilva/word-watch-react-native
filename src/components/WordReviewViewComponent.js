import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    ScrollView,
    ListView,
} from 'react-native';
import { styles } from '../styles.js';
import { WordViewComponent, LoadingViewComponent } from './WWComponents.js';

export class WordReviewViewComponent extends Component {
    constructor(props) {
        super(props);
        let words = new ListView.DataSource({rowHasChanged: (a, b) => a !== b});
        words = words.cloneWithRows([]);
        this.state = {words};
    }

    componentDidMount() {
        this.prepareList(this.props.words);
    }

    componentWillReceiveProps(newProps) {
        this.prepareList(newProps.words);
    }

    prepareList(words) {
        this.setState({
            words: this.state.words.cloneWithRows(words),
        });
    }

    renderRow(word) {
        return (
            <WordViewComponent word={word} />
        );
    }

    render() {
        if (this.props.querying) return (
            <LoadingViewComponent />
        );

        if ( ! this.props.words.length ) return (
            <View style={[styles.navBarMarginTop, {flex: 1, alignItems: 'center', justifyContent: 'center'}]}>
                <View style={{marginLeft: 30, marginRight: 30}}>
                    <Text style={{fontSize: 17, textAlign: 'center'}}>
                        {"You've yet to save some words to watch..."}
                    </Text>
                    <Text style={{fontSize: 17, textAlign: 'center', paddingTop: 20}}>
                        {"Use the menu on the left to 'Lookup' some words to watch!"}
                    </Text>
                </View>
            </View>
        );

        return (
            <ScrollView style={styles.navBarMarginTop}>
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.words}
                    renderRow={this.renderRow.bind(this)} />
                <View style={{height: 80}}></View>
            </ScrollView>
        );
    }
}
WordReviewViewComponent = connect(
    state => {
        return {
            querying: state.requests['FETCH_WORDS'],
            words: state.words,
        };
    }
)(WordReviewViewComponent);
