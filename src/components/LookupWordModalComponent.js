import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearWordResults, lookupWord, hideLookupWordModal } from '../actions.js';
import {
    Alert,
    Text,
    TextInput,
    View,
    TouchableOpacity,
    Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { styles, width } from '../styles.js';

export class LookupWordModalComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchModal: false,
            word: '',
        };
    }

    lookupWord() {
        if (this.state.word.length) this.props.lookupWord(this.state.word);
    }

    render() {
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.props.show}
                onRequestClose={() => {}}
                onShow={() => this.setState({word: ''})}>

                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <View style={{width: width/1.3, backgroundColor: 'teal', padding: 0}}>

                        <View style={{flex: 1, height: 70, flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{flex: 0.8, paddingTop: 20, paddingLeft: 20}}>
                                <Text style={styles.modalText}>Lookup Word</Text>
                            </View>
                            <TouchableOpacity onPress={this.props.hideLookupWordModal} style={{flex: 0.2, alignItems: 'center', justifyContent: 'center', paddingTop: 20, paddingRight: 20}}>
                                <View style={[styles.icon, {flex: 0}]}>
                                    <Icon name="close" size={25} color="#fff" />
                                </View>
                            </TouchableOpacity>
                        </View>
                        
                        <View style={{flex: 1, height: 70, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
                            <View style={{flex: 0.8, paddingBottom: 20, paddingLeft: 20}}>
                                <TextInput
                                    value={this.state.word}
                                    style={{color: 'white'}}
                                    placeholder={'Word...'}
                                    placeholderTextColor={'white'}
                                    onChangeText={ (word) => this.setState({word}) }
                                    underlineColorAndroid={'white'} />
                            </View>
                            <TouchableOpacity onPress={this.lookupWord.bind(this)} style={{flex: 0.2, alignItems: 'center', justifyContent: 'center', paddingRight: 20, paddingBottom: 20}}>
                                <View style={[styles.icon, {flex: 0}]}>
                                    <Icon name="search" size={25} color="#fff" />
                                </View>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </Modal>
        );
    }
}
LookupWordModalComponent = connect(
    state => {
        return {
            result: state.wordResults,
            show: state.lookupWordModalVisible,
        }
    },
    {
        clearWordResults,
        lookupWord,
        hideLookupWordModal,
    }
)(LookupWordModalComponent);
