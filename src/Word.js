export class Word {
    constructor(word, definitions, key) {
        this.word = word;
        this.definitions = definitions;
        this.key = key;
    }

    toObject() {
        return {
            word: this.word,
            definitions: this.definitions
        }
    }

    toCacheable() {
        return Object.assign(this.toObject(), {key: this.key});
    }
}
