import { Alert } from 'react-native';
import { lookup } from './api.js';
import * as db from './db.js';

const offlineAlert = function () {
    Alert.alert('Error', 'Word Watch is currently in offline mode. The request cannot be made.');
};

export const setUser = function (user) {
    return {type: 'SET_USER', user};
}

export const clearUser = function () {
    return {type: 'CLEAR_USER'};
}

export const setWordResults = function (words) {
    return {type: 'SET_WORD_RESULTS', words};
}

export const clearWordResults = function () {
    return {type: 'CLEAR_WORD_RESULTS'};
}

export const setWords = function (words) {
    return {type: 'SET_WORDS', words};
}

export const toggleDrawer = function () {
    return {type: 'TOGGLE_DRAWER'};
}

export const closeDrawer = function () {
    return {type: 'CLOSE_DRAWER'};
}

export const openDrawer = function () {
    return {type: 'OPEN_DRAWER'};
}

export const setRequestActive = function (request) {
    return {type: 'SET_REQUEST_ACTIVE', request};
}

export const setRequestComplete = function (request) {
    return {type: 'SET_REQUEST_COMPLETE', request};
}

export const showConfirmModal = function (text, cb) {
    return (dispatch) => {
        dispatch(closeDrawer());
        dispatch({type: 'SHOW_CONFIRM_MODAL', text, cb});
    }
}

export const hideConfirmModal = function () {
    return {type: 'HIDE_CONFIRM_MODAL'};
}

export const showLookupWordModal = function () {
    return (dispatch) => {
        dispatch(clearWordResults());
        dispatch(closeDrawer());
        dispatch({type: 'SHOW_LOOKUP_WORD_MODAL'});
    }
}

export const hideLookupWordModal = function () {
    return {type: 'HIDE_LOOKUP_WORD_MODAL'};
}

export const lookupWord = function (word) {
    return (dispatch, getState) => {
        let state = getState();
        if ( ! state.isConnected ) return offlineAlert();

        lookup(word).then(wordResults => {
            if ( ! wordResults ) return Alert.alert('Hmm...', 'Results not found for \'' + word + '\'.');
            dispatch(hideLookupWordModal());
            dispatch({type: 'SET_WORD_RESULTS', wordResults});
        });
    };
}

export const loadWords = function (user) {
    return (dispatch, getState) => {
        let { isConnected } = getState();

        dispatch(setRequestActive('FETCH_WORDS'));
        db.fetchWords(user, isConnected)
            .then(words => {
                dispatch(setRequestComplete('FETCH_WORDS'));
                dispatch(setWords(words));
                dispatch(clearWordResults());
            })
            .catch(err => {
                throw err;
            });
    };
}

export const login = function (email, password) {
    return (dispatch, getState) => {
        let { isConnected } = getState();

        db.login(email, password, isConnected)
            .then(user => {
                dispatch(setUser(user));
                dispatch(loadWords(user));
            })
            .catch(err => {
                Alert.alert('Error...', err.message);
            });
    };
}

export const register = function (email, password) {
    return (dispatch, getState) => {
        let state = getState();
        if ( ! state.isConnected ) return offlineAlert();

        db.register(email, password)
            .then(user => {
                dispatch(setUser(user));
            })
            .catch(err => {
                Alert.alert('Error...', err.message);
            });
    };
}

export const saveWord = function (word) {
    return (dispatch, getState) => {
        let state = getState();
        if ( ! state.isConnected ) return offlineAlert();

        let { user } = state;
        if ( ! user ) return Alert.alert('Error...', 'Word cannot be saved. User not logged in.');

        db.saveWord(user, word)
            .then(() => {
                dispatch(loadWords(user));
            })
            .catch(err => {
                throw err;
            });
    };
}

export const removeWord = function (word) {
    return (dispatch, getState) => {
        let state = getState();
        if ( ! state.isConnected ) return offlineAlert();

        let { user } = state;
        if ( ! user ) return Alert.alert('Error...', 'Word cannot be removed. User not logged in.');

        db.removeWord(user, word)
            .then(() => {
                dispatch(loadWords(user));
            })
            .catch(err => {
                Alert.alert('Error...', err.message);
            });
    };
}
