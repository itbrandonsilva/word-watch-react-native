import { Platform, Navigator } from 'react-native';

export function configureScene() {
    if (Platform.OS == 'android') return Navigator.SceneConfigs.FadeAndroid;
    else return Navigator.SceneConfigs.PushFromRight;
}
