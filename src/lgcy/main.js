/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import app from './app.js';
import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import Drawer from 'react-native-drawer';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    Image,
    View,
    Alert,
    TouchableOpacity,
    TouchableHighlight,
    Navigator,
    Platform,
    ScrollView,
    ListView,
    BackAndroid,
    Dimensions,
    Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import events from './events.js';
import { lookup } from './api.js';

let {height, width} = Dimensions.get('window');

function configureScene() {
    if (Platform.OS == 'android') return Navigator.SceneConfigs.FadeAndroid;
    else return Navigator.SceneConfigs.PushFromRight;
}

BackAndroid.addEventListener('hardwareBackPress', () => {
    events.emit('toggleDrawer');
    return true;
    //if (navigator && navigator.getCurrentRoutes().length > 1) {
    //    navigator.pop();
    //    return true;
    //}
    //return false;
});

export class RootComponent extends Component {
    render() {
        return (
            <Provider store={app}>
                <ViewSelectorComponent />
            </Provider>
        );
    }
}

class ViewSelectorComponent extends Component {
    render() {
        let view = this.props.user ? <AppViewComponent /> : <LoginViewComponent />;
        return view;
    }
}
ViewSelectorComponent = connect(
    (state) => {
        return {
            user: state.user
        }
    }
)(ViewSelectorComponent);


class LoginViewComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: 'email@email.com',
            password: 'password',
            passwordConfirmation: 'password',
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.user) this._navigator.resetTo({scene: 'login'});
    }

    renderLogin(route, navigator) {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.homeLogoContainer}>
                    <View style={styles.flexCenter}>
                        <Image style={styles.homeLogo} resizeMode={Image.resizeMode.contain} source={require('./assets/logo.png')} />
                    </View>
                </View>

                <View style={styles.homeInputContainer}>
                    <TextInput
                        placeholder = {'Email Address'}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (email) => this.setState({email}) }
                        value={this.state.email}/>

                    <TextInput
                        placeholder = {'Password'}
                        secureTextEntry = {true}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (password) => this.setState({password}) }
                        value={this.state.password}/>

                    <View style={[styles.flexCenterHorizontal, {paddingTop: 20}]}>
                        <TouchableOpacity 
                            onPress={() => {this.props.submit(this.state.email, this.state.password)}}
                            style={styles.button}>
                            <Text>Login</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={() => navigator.push({scene: 'register'})}
                            style={styles.button}>
                            <Text>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }

    renderRegister(route, navigator) {
        return (
            <ScrollView style={styles.container}>
                <View style={{backgroundColor: 'teal', width, height: Navigator.NavigationBar.Styles.General.NavBarHeight}}>
                    <View style={styles.registerHeader}>
                        <Text style={styles.navBarText}>
                            Register
                        </Text>
                    </View>
                </View>
                <View style={styles.homeInputContainer}>
                    <TextInput
                        placeholder = {'Email Address'}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (email) => this.setState({email}) }
                        value={this.state.email}/>

                    <TextInput
                        placeholder = {'Password'}
                        secureTextEntry = {true}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (password) => this.setState({password}) }
                        value={this.state.password}/>

                    <TextInput
                        placeholder = {'Confirm Password'}
                        secureTextEntry = {true}
                        editable = {true}
                        maxLength = {40}
                        onChangeText={ (passwordConfirmation) => this.setState({passwordConfirmation}) }
                        value={this.state.passwordConfirmation}/>


                    <View style={[styles.flexCenterHorizontal, {paddingTop: 20}]}>
                        <TouchableOpacity 
                            onPress={() => this.state.password === this.state.passwordConfirmation ? this.props.submitRegister(this.state.email, this.state.password) : alert('Passwords do not match.')}
                            style={styles.button}>
                            <Text>Create Account</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={() => navigator.pop()}
                            style={styles.button}>
                            <Text>Back</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )

    }

    renderScene(route, navigator) {
        switch (route.scene) {
            case 'login':
                return this.renderLogin(route, navigator);
            case 'register':
                return this.renderRegister(route, navigator);
            default:
                return null;
        }
    }

    render() {
        return (
            <Navigator
                ref={(ref) => this._navigator = ref}
                initialRoute={this.props.route}
                configureScene={configureScene}

                renderScene={this.renderScene.bind(this)} />
        );
    }
}
LoginViewComponent = connect(
    (state) => {
        return {
            route: state.route,
            user: state.user,
        }
    },
    (dispatch, props) => new Object({
        submit: (email, password) => {
            dispatch({type: 'LOGIN', email, password});
        },
        submitRegister: (email, password) => {
            dispatch({type: 'REGISTER', email, password});
        }
    })
)(LoginViewComponent);

class AppViewComponent extends Component {
    constructor(props) {
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (a, b) => a !== b});
        this.state = {
            word: 'complete',
            result: [],
            dataSource: ds,
            showSearchModal: false,
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.user !== this.props.user) {
            if (newProps.user) return this._navigator.push({scene: 'lookup'});
            this._drawer.close();
            this._navigator.pop();
        }
    }

    renderDrawer() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={(rowData) =>
                    <TouchableOpacity onPress={() => {this.props.logout()}} style={{flex: 1, height: 60, justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1}}>
                        <Text>{rowData}</Text>
                    </TouchableOpacity>
                }
            />
        );
    }

    renderNavigationBar() {
        return <NavBarComponent />;
    }

    sceneRenderer(route, navigator) {
        let saveWordButton = this.state.result.length ? (
            <TouchableOpacity 
                onPress={this.props.saveWord}
                style={styles.button}>
                <Text>Save Word</Text>
            </TouchableOpacity>
        ) : undefined;

        let routeView = ((() => {
            switch (route.scene) {
                case 'wordWatch': return (<WordWatchViewComponent />);
                default: return null;
            }
        })());

        return (
            <Drawer
                ref={(ref) => {this._drawer = ref}}
                //type={"overlay"}
                type={"displace"}
                content={this.renderDrawer()}
                tapToClose={true}
                openDrawerOffset={0.65} // 30% gap on the right side of drawer
                panCloseMask={0.65}
                closedDrawerOffset={0}
                styles={drawerStyles}
                side={'left'}
                tweenDuration={250}
                tweenEasing={'easeOutQuint'}
                captureGestures={false}>

                { routeView }
            </Drawer>
        );

                        //<Icon.Button style={{borderRadius: 0, flex: 0.8}} name="search" backgroundColor="teal" onPress={() => events.emit('showLookupModal')}>
                        //    Lookup Word
                        //</Icon.Button>


                    //<Text style={styles.welcome}>
                    //    Hello, {this.props.user.email}
                    //</Text>


                        //<TouchableOpacity onPress={() => events.emit('showLookupModal')} style={[styles.button, {flex: 0.8}]}>
                        //    <Text>Search</Text>
                        //</TouchableOpacity>

                    //<TouchableOpacity onPress={() => this.props.lookupWord(this.state.word)} style={[styles.button, {flex: 0.35}]}>
                        //<View style={{flex: 0.35, justifyContent: 'center'}}>
                        //</View>

                    //<TouchableHighlight onPress={() => {this.setState({showSearchModal: false})}}>
                    //  <Text style={styles.modalText}>Hide Modal</Text>
                    //</TouchableHighlight>


    }

    componentDidMount() {
        events.on('openDrawer', this.openDrawer.bind(this));
        events.on('toggleDrawer', this.toggleDrawer.bind(this));

        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(['Word Watch', 'Profile', 'Settings', 'Games', 'Logout'])
        });
    }

    componentWillUnmount() {
        events.removeAllListeners('openDrawer');
        events.removeAllListeners('toggleDrawer');
    }

    openDrawer() {
        this._drawer.open();
    }

    toggleDrawer() {
        let d = this._drawer;
        d._open ? d.close() : d.open();
    }

    render() {
        return (
            <Navigator
                ref={(ref) => this._navigator = ref}
                initialRoute={{scene: 'wordWatch'}}
                configureScene={configureScene}

                navigationBar={this.renderNavigationBar()}
                renderScene={this.sceneRenderer.bind(this)} />
        );
    }
}
AppViewComponent = connect(
    (state) => state,
    (dispatch) => {
        return {
            lookupWord: (word) => dispatch({type: 'LOOKUP_WORD', word}),
            logout: () => dispatch({type: 'LOGOUT'}),
        }
    }
)(AppViewComponent);

class WordWatchViewComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderWordResults() {
        if ( ! this.state.wordResult ) return null;
        return <WordViewComponent word={this.state.wordResult} />
    }

    render() {
        return (
            <ScrollView style={styles.navBarMarginTop}>
                <LookupWordModalComponent results={(wordResult) => this.setState({wordResult})}/>

                <View style={{flexDirection: 'column', marginTop: 10}}>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 0.15}}></View>

                        <TouchableHighlight style={{flex: 0.7, backgroundColor: 'teal', borderRadius: 6}} onPress={() => events.emit('showLookupModal')}>
                            <View style={[styles.flexCenter, {padding: 8}]}>
                                <Icon name={'search'} size={20} style={{color: 'white', marginRight: 10}} />
                                <Text style={{color: 'white', fontWeight: '600'}}>
                                    Lookup Word
                                </Text>
                                <View></View>
                            </View>
                        </TouchableHighlight>

                        <View style={{flex: 0.15}}></View>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <View style={{flex: 0.15}}></View>

                        <TouchableHighlight style={{flex: 0.7, backgroundColor: 'teal', borderRadius: 6}} onPress={() => events.emit('showLookupModal')}>
                            <View style={[styles.flexCenter, {padding: 8}]}>
                                <Icon name={'book'} size={20} style={{color: 'white', marginRight: 10}} />
                                <Text style={{color: 'white', fontWeight: '600'}}>
                                    Review Words
                                </Text>
                                <View></View>
                            </View>
                        </TouchableHighlight>

                        <View style={{flex: 0.15}}></View>
                    </View>
                </View>

                { this.renderWordResults() }
            </ScrollView>
        );

                //{ saveWordButton }
                //{ wordViews }

    }
}

class LookupWordModalComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchModal: false,
            word: '',
        };
    }

    componentDidMount() {
        events.on('showLookupModal', () => this.setState({showSearchModal: true}));
    }

    componentWillUnmount() {
        events.removeAllListeners('showLookupModal');
    }

    lookupWord() {
        if ( ! this.state.word || ! this.state.word.length ) return Alert.alert('', 'Enter the word you\'d like to lookup.');
        lookup(this.state.word)
            .then((result) => {
                if ( ! Object.keys(result.def).length ) return Alert.alert('', 'No definition found for ' + this.state.word);
                this.props.results(result);
                this.setState({showSearchModal: false});
            });
    }

    render() {
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.state.showSearchModal}
                onRequestClose={() => {}}>

                <View style={styles.flexCenter}>
                    <View style={styles.modal}>
                        <View style={{flexDirection: 'column', justifyContent: 'center'}}>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={[{flex: 0.8}, styles.modalText]}>Lookup Word</Text>
                                <TouchableOpacity onPress={() => this.setState({showSearchModal: false})} style={{flex: 0.2, flexDirection: 'row', justifyContent: 'flex-end'}}>
                                    <View style={[styles.icon, {paddingTop: 0, paddingRight: 0}]}>
                                        <Icon name="close" size={25} color="#fff" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                                <TextInput
                                    value={this.state.word}
                                    style={{flex: 0.8, color: 'white'}}
                                    placeholder={'Word...'}
                                    placeholderTextColor={'white'}
                                    onChangeText={ (word) => this.setState({word}) }
                                    underlineColorAndroid={'white'}/>
                                <TouchableOpacity onPress={this.lookupWord.bind(this)} style={{flex: 0.2, flexDirection: 'row', justifyContent: 'flex-end'}}>
                                    <View style={[styles.icon, {paddingBottom: 0, paddingRight: 0}]}>
                                        <Icon name="search" size={25} color="#fff" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>

            </Modal>
        );
    }
}
LookupWordModalComponent = connect()(LookupWordModalComponent);

class WordViewComponent extends Component {
    render() {
        var word = this.props.word;

        let views = [];
        Object.keys(word.def).forEach((partOfSpeech, idx) => {
            var definitionViews = [];
            let definitions = word.def[partOfSpeech];
            definitions.forEach((definition, idx) => {
                definitionViews.push(
                    <Text key={idx}>- { definition }</Text>
                );
            });
            views.push(
                <View key={idx} style={{margin: 15, marginBottom: 0}}>
                    <Text style={{fontSize: 22}}>
                        { partOfSpeech }
                    </Text>
                    { definitionViews }
                </View>
            )
        });

        return (
            <View>
                <View style={{marginTop: 20, flexDirection: 'row', justifyContent: 'center'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity
                            //onPress={() => events.emit('toggleDrawer')}
                            style={styles.icon}>
                            <Text>
                                <Icon name={'save'} size={30} color={'teal'} />
                            </Text>
                        </TouchableOpacity>

                        <Text style={{fontSize: 30, fontWeight: 'bold'}}>
                            { word.word }
                        </Text>
                    </View>
                </View>
                { views }
            </View>
        );
    }
}
WordViewComponent = connect(

)(WordViewComponent);

class NavBarComponent extends Component {
    render() {
        var currentRoute = this.props.navigator.getCurrentRoutes().pop();
        if (! this.props.user) return null;
        return <Navigator.NavigationBar
            {...this.props}
            routeMapper={{
                LeftButton: (route, navigator, index, navState) => {
                    return (
                        <TouchableOpacity
                            onPress={() => events.emit('toggleDrawer')}
                            style={styles.icon}>
                            <Text>
                                <Icon name="bars" size={30} color="#fff" />
                            </Text>
                        </TouchableOpacity>
                    )
                },
                Title: (route, navigator, index, navState) => {
                    return (
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={styles.navBarText}>
                                Word Watch
                            </Text>
                        </View>
                    );
                },
                RightButton: (route, navigator, index, navState) => {
                    return null;
                },
            }}
            style={{backgroundColor: 'teal'}} />

    }
}
NavBarComponent = connect(
    (state) => state
)(NavBarComponent);
                    

const styles = StyleSheet.create({
    home: {
        zIndex: 2000,
    },
    homeLogoContainer: {
        width,
        height: height/2,
        backgroundColor: 'teal',
    },
    // No idea why this works the way it does...but these styles are necessary for resizeMode='contain' to work
    homeLogo: {
        flex: 1,
        width: width/2,
    },
    homeInputContainer: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    registerHeader: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 20,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInput: {
    
    },
    button: {
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    flexCenter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexCenterVertical: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    flexCenterHorizontal: {
        flex: 1,
        justifyContent: 'center',
    },
    navBarMarginTop: {
        paddingTop: Navigator.NavigationBar.Styles.General.NavBarHeight,
    },
    navBarText: {
        color: 'white',
        fontSize: 20,
    },
    modal: {
        padding: 20,
        backgroundColor: 'teal',
        width: width/1.3,
    },
    modalText: {
        color: 'white',
        fontSize: 20,
    },
});

const drawerStyles = {
    drawer: {
        top: Navigator.NavigationBar.Styles.General.NavBarHeight,
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        backgroundColor: 'teal',
    },
};
