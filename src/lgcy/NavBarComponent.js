import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleDrawer } from '../actions.js';
import {
    Text,
    View,
    TouchableOpacity,
    Navigator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from '../styles.js';

// https://github.com/facebook/react-native/issues/2560

export class NavBarComponent extends Navigator.NavigationBar {
    constructor(props) {
        super(props);
        this.routeMapper = {
            LeftButton: (route, navigator, index, navState) => {
                if (route.scene === 'result') { return (
                    <TouchableOpacity
                        onPress={() => navigator.pop()}
                        style={styles.icon}>
                        <Text>
                            <Icon name="chevron-left" size={30} color="white" />
                        </Text>
                    </TouchableOpacity>
                ); }
                else { return (
                    <TouchableOpacity
                        onPress={this.props.toggleDrawer}
                        style={styles.icon}>
                        <Text>
                            <Icon name="bars" size={30} color="#fff" />
                        </Text>
                    </TouchableOpacity>
                ); }
            },
            Title: (route, navigator, index, navState) => {
                return (
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={styles.navBarText}>
                            Word Watch
                        </Text>
                    </View>
                );
            },
            RightButton: (route, navigator, index, navState) => {
                return null;
            },
        };
    }

    render() {
        return (
            <Navigator.NavigationBar
                style={{backgroundColor: 'teal'}}
                routeMapper={this.routeMapper} />
        );
    }
}
NavBarComponent = connect(
    undefined,
    {
        toggleDrawer
    }
)(NavBarComponent);
