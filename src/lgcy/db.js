import { Alert } from 'react-native';
import * as firebase from 'firebase';
import store from './store.js';

import db from './db.js';

var config = { 
    apiKey: "AIzaSyAWWpNFn9Fy3dfkjx5PEihtnFNYv7XDcaM",
    authDomain: "word-watch.firebaseapp.com",
    databaseURL: "https://word-watch.firebaseio.com",
    storageBucket: "word-watch.appspot.com",
};
firebase.initializeApp(config);

export const reloadWords = function (user) {
    if ( ! user ) throw new Error('User currently not logged in');
    var ref = firebase.database().ref('/user/' + user.uid + '/words');
    ref.on('value', (snapshot, err) => {
        if (err) throw err;

        var words = [];
        snapshot.forEach(word => {
            let val = word.val();
            val.json = JSON.parse(val.json);
            val.json.key = word.key;
            words.push(val);
        });
        setTimeout(() => {
            store.dispatch({type: 'SET_WORDS', words});
        }, 1);
    });
}

function subscribeWords(user) {

}

export const login = function (email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then((user) => {
        store.dispatch({type: 'SET_USER', user});
    }).catch((err) => {
        Alert.alert('Error...', err.message);
    });
}

export const register = function (email, password) {
    firebase.auth().createUserWithEmailAndPassword(email, password) 
    .then((user) => {
        store.dispatch({type: 'SET_USER', user});
    }).catch((err) => {
        Alert.alert('Error...', err.message);
    });
}

export const saveWord = function (user, word) {
    var ref = firebase.database().ref('/user/' + user.uid + '/words');
    ref.push({
        word: word.word,
        json: JSON.stringify(word),
    }, err => {
        if (err) throw err;
        store.dispatch({type: 'RELOAD_WORDS'});
    });
}

export const removeWord = function (user, word) {
    var ref = firebase.database().ref('/user/' + user.uid + '/words/' + word.key);
    ref.remove()
    .then(() => {
        store.dispatch({type: 'RELOAD_WORDS'});
    })
    .catch((err) => {
        Alert.alert('Error...', err.message);
    });
}
