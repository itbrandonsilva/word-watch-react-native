import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    TouchableHighlight,
    ScrollView,
    Navigator,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import events from '../events.js';
import { configureScene } from '../util.js';
import { styles } from '../styles.js';
import { WordViewComponent, LookupWordModalComponent } from './WWComponents.js';

export class WordWatchViewComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderWordResults() {
        if ( ! this.state.wordResult ) return null;
        return <WordViewComponent word={this.state.wordResult} />
    }

    componentDidMount() {
        events.on('set-route-home', () => {
            this._navigator.popToTop();
            events.emit('toggleDrawer');
        });
    }

    componentWillUnmount() {
        events.removeAllListeners('set-route-home');
    }

    renderScene(route, navigator) {
        switch (route.scene) {
            case 'home':
                return (
                    <ScrollView style={styles.navBarMarginTop}>
                        <LookupWordModalComponent results={(wordResult) => this.setState({wordResult})}/>

                        <View style={{flexDirection: 'column', marginTop: 10}}>
                            <View style={{flexDirection: 'row', marginTop: 10}}>
                                <View style={{flex: 0.15}}></View>

                                <TouchableHighlight style={{flex: 0.7, backgroundColor: 'teal', borderRadius: 6}} onPress={() => events.emit('showLookupModal')}>
                                    <View style={[styles.flexCenter, {padding: 8}]}>
                                        <Icon name={'search'} size={20} style={{color: 'white', marginRight: 10}} />
                                        <Text style={{color: 'white', fontWeight: '600'}}>
                                            Lookup Word
                                        </Text>
                                        <View></View>
                                    </View>
                                </TouchableHighlight>

                                <View style={{flex: 0.15}}></View>
                            </View>

                            <View style={{flexDirection: 'row', marginTop: 10}}>
                                <View style={{flex: 0.15}}></View>

                                <TouchableHighlight style={{flex: 0.7, backgroundColor: 'teal', borderRadius: 6}} onPress={() => navigator.push({scene: 'review'})}>
                                    <View style={[styles.flexCenter, {padding: 8}]}>
                                        <Icon name={'book'} size={20} style={{color: 'white', marginRight: 10}} />
                                        <Text style={{color: 'white', fontWeight: '600'}}>
                                            Review Words
                                        </Text>
                                        <View></View>
                                    </View>
                                </TouchableHighlight>

                                <View style={{flex: 0.15}}></View>
                            </View>
                        </View>

                        { this.renderWordResults() }
                    </ScrollView>
                );
            case 'review':
                let words = this.props.words.map((word, idx) => {
                    return (
                        <View key={idx}>
                            <Text>{ word.word }</Text>
                        </View>
                    );
                });
                return (
                    <ScrollView style={styles.navBarMarginTop}>
                        <Text>Review</Text>
                        { words }
                    </ScrollView>
                );
        }
    }

    render() {
        return (
            <Navigator
                ref={ref => this._navigator = ref}
                initialRoute={{scene: 'home'}}
                configureScene={configureScene}
                renderScene={this.renderScene.bind(this)} />
        );
    }
}
WordWatchViewComponent = connect(
    state => {
        return {
            words: state.words,
        };
    }
)(WordWatchViewComponent);
